<?php

namespace Database\Seeders;

use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('1234567890'),
            'admin' => 1,
            'phoneNumber' => 123456789
        ]);
    }
}
