<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\NovelController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\FrontFndController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/products', [ProductController::class, 'index'])->name('products');
});

Route::prefix('products')->group(function () {
    Route::get('/create', [ProductController::class, 'create']);
    Route::get('/edit/{id}', [ProductController::class, 'edit']);
    Route::get('/delete/{id}', [ProductController::class, 'destroy']);
    Route::post('/update/{id}', [ProductController::class, 'update']);
    Route::post('/store', [ProductController::class, 'store']);
    Route::get('/search', [ProductController::class, 'search']);
    Route::post('/add_to_cart', [ProductController::class, 'addToCart']);
    Route::get('/cartlist', [ProductController::class, 'cartList']);
    Route::get('/removecart/{id}', [ProductController::class, 'removeCart']);
    Route::get('/ordernow', [ProductController::class, 'orderNow']);
    Route::post('/orderplace', [ProductController::class, 'orderPlace']);
    Route::get('/orders', [ProductController::class, 'order']);

    });



Route::prefix('categories')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])->name('categories');
    Route::get('/create', [CategoryController::class, 'create']);
    Route::get('/edit/{id}', [CategoryController::class, 'edit']);
    Route::get('/delete/{id}', [CategoryController::class, 'destroy']);
    Route::post('/update/{id}', [CategoryController::class, 'update']);
    Route::post('/store', [CategoryController::class, 'store']);
       
    });

Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('users');
        Route::get('/create', [UserController::class, 'create']);
        Route::get('/edit/{id}', [UserController::class, 'edit']);
        Route::get('/delete/{id}', [UserController::class, 'destroy']);
        Route::post('/update/{id}', [UserController::class, 'update']);
        Route::post('/store', [UserController::class, 'store']);
           
        });
    


    Route::get('/index', [FrontFndController::class, 'index']);
    Route::get('/product/{id}',[FrontFndController::class, 'show'])->name('product.show');



    