@extends('layouts.app')

@section('content')
     <form action="/users/update/{{$users->id}}" method="POST" enctype="multipart/form-data">
         @csrf
         <div class="container pt-5">
            <h1>{{ __('Edit Users')}}</h1>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"  value="{{ $users->name }}" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"  value="{{ $users->email }}">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                                <label for="phone number" class="col-md-4 col-form-label text-md-end">{{ __('Phone Number') }}</label>
    
                                <div class="col-md-6">
                                    <input id="phoneNumber" type="text" class="form-control" name="phoneNumber" value="{{ $users->phoneNumber }}">
                                </div>
                        </div>

             <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection