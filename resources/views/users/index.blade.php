@extends('layouts.app')

@section('content')
    <div class="container pt-5">
    <div class="panel panel-default">
                <div class="panel-heading">
                    All Users
                    <a href="/users/create" class="btn btn-outline-secondary btn-sm"><i class="fa-solid fa-plus"></i></a>
                </div>
                <div class="panel-body">
                <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">User Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if($users->count()>0)
                                @foreach($users as $user)
                                    <tr>        
                                        <td>
                                            <label for="name">{{ __($user->name)}} </label>
                                        </td>
                                        <td>
                                            <label for="email">{{ __($user->email)}} </label>
                                        </td>
                                        <td>
                                            <label for="phoneNumber">{{ __($user->phoneNumber)}} </label>
                                        </td>
                                        <td>
                                           <a href="/users/edit/{{$user->id}}"class="btn btn-outline-info"><i class="fa-solid fa-pen"></i></a>
                                        </td>
                    
                                        <td>
                                            <a href="/users/delete/{{$user->id}}"class="btn btn-outline-danger"><i class="fa-solid fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <tr>
                                @endforeach
                            @else
                                    <th colspan="4" class="text-center">No Users published</th>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>   
    </div>
@endsection

        