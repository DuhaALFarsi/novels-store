@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
       <div class="col">
            <img src="{{ asset($product->image_path) }}" alt="" height="400px" width="300px">
       </div>
       <div class="col-8">
            <div class="row py-3 px-lg-5">
                <h1 class="h1">{{ $product->name }}</h1>
            </div>
            <div class="row py-3 px-lg-5">
                <h1 class="h3">{{ $product->category->name}}</h1>
            </div>
            <div class="row py-3 px-lg-5">
                <div class="h3 text-muted">{{ $product->price}} OMR</div>
            </div>
       </div>
   </div>
@endsection