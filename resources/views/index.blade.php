@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
        <form class="d-flex mp-3"action="/products/search">
            <input class="form-control" name="query" type="text" placeholder="Search" aria-label="Search Product">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
            @foreach($products as $product)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 mt-3">
                    <div class="card">
                        <div class="card-img-top">
                            <img src="{{asset($product->image_path)}}" alt="novel" width="100%"  height="400px" class="rounded mx-auto d-block">
                        </div>

                        <div class="card-body">
                            <a href="{{route('product.show', ['id' => $product->id]) }}}}" style="text-decoration:none;">
                                <h5 class="card-title h3 text-center" >
                                    {{ $product->name }}
                                </h5>
                            </a>
                            <h6 class="card-title h5 text-center" >
                                    {{ $product->category->name}}
                                </h6>
                            <div class="card-text h3 text-center text-muted">{{ $product->price }} OMR</div>
                        </div>
                        
                        <form action="/products/add_to_cart" method="POST">
                            @csrf
                                <input type="hidden" name="product_id" value={{$product['id']}}>
                                <div class="d-grid gap-2 col-6 mx-auto">
                                <button  class="btn btn-primary mx-5">Add to Cart</button>
                                </div>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
        
        <div class="row">
            <div class="col">
                <div class="text-center">
                    <div class="col-lg-12">{{ $products->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@endsection