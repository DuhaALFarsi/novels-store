
@extends('layouts.app')

@section('content')
    <div class="container pt-5">
    <div class="panel panel-default">
                <div class="panel-heading">
                    All Orders
                </div>
                <div class="panel-body">
                <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">product ID</th>
                            <th scope="col">User ID</th>
                            <th scope="col">Payment Method</th>
                            <th scope="col">Payment Status</th>
                            <th scope="col">Address</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if($orders->count()>0)
                                @foreach($orders as $order)
                                    <tr>        
                                        <td>
                                          {{ __($order->product_id)}} 
                                        </td>
                                        <td>
                                        {{ __($order->user_id)}} 
                                        <td>
                                         {{ __($order->payment_method)}}</label>
                                        </td>
                                        <td>
                                        {{__($order->payment_status)}}
                                        </td>
                                        <td>
                                        {{__($order->address)}}
                                        </td>
                                    </tr>
                                <tr>
                                @endforeach
                            @else
                                    <th colspan="4" class="text-center">No orders published</th>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>   
    </div>
@endsection

        