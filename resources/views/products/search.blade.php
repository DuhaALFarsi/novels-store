
@extends('layouts.app')

@section('content')
    <div class="container pt-5">
    <div class="panel panel-default">
                <div class="panel-heading">
                    Search Page
                </div>
                <div class="panel-body">
                <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">product Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Price</th>
                            <th scope="col">Category ID</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if($products->count()>0)
                                @foreach($products as $product)
                                    <tr>        
                                        <td>
                                          {{ __($product->name)}} 
                                        </td>
                                        <td>
                                        <img src="{{asset($product->image_path)}}" alt="" width="60px" height="60px"></td>

                                        <td>
                                            <label for="price">{{ __($product->price)}}</label>
                                        </td>
                                        <td>
                                        <label for="category_id">{{ __($product->category_id)}}</label>
                                        </td>
                                    </tr>
                                <tr>
                                @endforeach
                            @else
                                    <th colspan="4" class="text-center">No product</th>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>   
    </div>
@endsection

        