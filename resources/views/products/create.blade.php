@extends('layouts.app')

@section('content')
    
     <form action="/products/store" method="POST" enctype="multipart/form-data">
         @csrf
         <div class="container pt-5">
            <h1>{{ __('Create Novels') }}</h1>
         <div class="mb-3">
             <label for="name" class="form-label">{{ __('Name')}}</label>
             <input type="text" class="form-control" id="name" name="name">
           </div>

           <div class="mb-3">
             <label for="price" class="form-label">{{ __('Price')}}</label>
             <input type="number" class="form-control" id="price" name="price">
             </div>
     
             <div class="mb-3">
                 <label for="formFile" class="form-label">{{ __('Novel Image')}}</label>
                 <input class="form-control" type="file" id="formFile" name="image">
               </div>
     
               <div class="mb-3">
                 <label for="category_id" class="form-label">{{ __('Category Name')}}</label>

                   <select name="category_id" id="" class="form-control">
                   @foreach ($categories as $category)
                   <option value="{{$category->id}}">{{$category->name}}</option>
                   @endforeach
                  </select>
                </div>

                 <button type="submit" class="btn btn-primary">Submit</button>
        
    </form>
@endsection