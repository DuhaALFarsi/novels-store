
@extends('layouts.app')

@section('content')
    <div class="container pt-5">
    <div class="panel panel-default">
                <div class="panel-heading">
                    Products in Cart
                </div>
                <div class="panel-body">
                <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">product Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Price</th>
                            <th scope="col">Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if($products->count()>0)
                                @foreach($products as $product)
                                    <tr>        
                                        <td>
                                          {{ __($product->name)}} 
                                        </td>
                                        <td>
                                        <img src="{{asset($product->image_path)}}" alt="" width="60px" height="60px">
                                        </td>

                                        <td>
                                            <label for="price">{{ __($product->price)}}</label>
                                        </td>
                                        <td>
                                            <a  href="/products/removecart/{{$product->cart_id}}" class="btn btn-warning">Remove</a>
                                        </td>
                                    </tr>
                                <tr>
                                @endforeach
                            @else
                                    <th colspan="4" class="text-center">No product in cart</th>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                    <a  href="/products/ordernow" class="btn btn-success">Order Now</a>
                </div>
            </div>   
    </div>
@endsection

        