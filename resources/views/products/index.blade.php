
@extends('layouts.app')

@section('content')
    <div class="container pt-5">
    <div class="panel panel-default">
                <div class="panel-heading">
                    All Novels
                    <a href="/products/create" class="btn btn-outline-secondary btn-sm"><i class="fa-solid fa-plus"></i></a>
                </div>
                <div class="panel-body">
                <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">product Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Price</th>
                            <th scope="col">Category Name</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if($products->count()>0)
                                @foreach($products as $product)
                                    <tr>        
                                        <td>
                                          {{ __($product->name)}} 
                                        </td>
                                        <td>
                                        <img src="{{asset($product->image_path)}}" alt="" width="60px" height="60px"></td>

                                        <td>
                                            <label for="price">{{ __($product->price)}}</label>
                                        </td>
                                        <td>
                                        <label for="category_id">{{ __($product->category->name)}}</label>
                                        </td>
                                        <td>
                                           <a href="/products/edit/{{$product->id}}"class="btn btn-outline-info"><i class="fa-solid fa-pen"></i></a>
                                        </td>
                    
                                        <td>
                                            <a href="/products/delete/{{$product->id}}"class="btn btn-outline-danger"><i class="fa-solid fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <tr>
                                @endforeach
                            @else
                                    <th colspan="4" class="text-center">No product published</th>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>   
    </div>
@endsection

        