@extends('layouts.app')

@section('content')
     <form action="/products/update/{{$products->id}}" method="POST" enctype="multipart/form-data">
         @csrf
         <div class="container pt-5">
            <h1>{{ __('Edit Products')}}</h1>
         <div class="mb-3">
             <label for="name" class="form-label">{{ __('Name')}}</label>
             <input type="text" class="form-control" id="name" name="name" value="{{ $products->name }}">
           </div>

           <div class="mb-3">
             <label for="price" class="form-label">{{ __('Price')}}</label>
             <input type="number" class="form-control" id="price" name="price" value="{{ $products->price }}">
             </div>
     
             <div class="mb-3">
                 <label for="formFile" class="form-label">{{ __('Product Image')}}</label>
                 <input class="form-control" type="file" id="formFile" name="image" >
                 <br>
                 <img src="{{ asset($products->image_path) }}" alt="" style="max-width: 100px">
               </div>
     
               <div class="mb-3">
                <label for="category_id" class="form-label">{{ __('Category Name')}}</label>

                  <select name="category_id" id="" class="form-control">
                  @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                 </select>
               </div>

                 <button type="submit" class="btn btn-primary">Submit</button>
         </div>
    </form>
@endsection