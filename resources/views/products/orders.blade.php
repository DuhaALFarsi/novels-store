
@extends('layouts.app')

@section('content')
    <div class="container pt-5">
        <h1>Payment Page</h1>
         <table class='table'>
        <tbody>
          <tr>
            <td>Amount</td>
            <td>{{$total}} OMR</td>
          </tr>
          <tr>
            <td>Tax</td>
            <td>0 OMR</td>
          </tr>
          <tr>
            <td>Delivery</td>
            <td>2 OMR</td>
          </tr>
          <tr>
            <td>Total Amount</td>
            <td>{{$total + 2}} OMR</td>
          </tr>
         </tbody>
        </table>
        <div>
        <form action="/products/orderplace" method="POST">
        @csrf
        <div class="mb-3">
            <textarea class="form-control is-invalid" name="address" placeholder="Please Enter Your Address" required></textarea>
        </div>
        <div>
            <label for="payment">Payment Method</label> <br> <br>
            <input type="radio" value="online" name="payment"> Online Payment <br> <br>
            <input type="radio" value="cash" name="payment"> Payment on Delivery <br> <br>
        </div>
        <button type="submit" class="btn btn-primary">Order Now</button>
      </form>
        </div>
    </div>
@endsection

        