@extends('layouts.app')

@section('content')
    <div class="container pt-5">
    <div class="panel panel-default">
                <div class="panel-heading">
                    All categories
                    <a href="/categories/create" class="btn btn-outline-secondary btn-sm"><i class="fa-solid fa-plus"></i></a>
                </div>
                <div class="panel-body">
                <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">Category Name</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if($categories->count()>0)
                                @foreach($categories as $Category)
                                    <tr>        
                                        <td>
                                          {{ __($Category->name)}} 
                                        </td>
                                        <td>
                                           <a href="/categories/edit/{{$Category->id}}"class="btn btn-outline-info"><i class="fa-solid fa-pen"></i></a>
                                        </td>
                    
                                        <td>
                                            <a href="/categories/delete/{{$Category->id}}"class="btn btn-outline-danger"><i class="fa-solid fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <tr>
                                @endforeach
                            @else
                                    <th colspan="4" class="text-center">No Category published</th>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>   
    </div>
@endsection

        