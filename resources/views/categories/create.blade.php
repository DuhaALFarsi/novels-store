@extends('layouts.app')

@section('content')
    
     <form action="/categories/store" method="POST" enctype="multipart/form-data">
         @csrf
         <div class="container pt-5">
            <h1>{{ __('Create category') }}</h1>
         <div class="mb-3">
             <label for="name" class="form-label">{{ __('Name')}}</label>
             <input type="text" class="form-control" id="name" name="name">
           </div>
         <button type="submit" class="btn btn-primary">Submit</button>
        
    </form>
@endsection