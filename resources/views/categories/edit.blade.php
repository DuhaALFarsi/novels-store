@extends('layouts.app')

@section('content')
     <form action="/categories/update/{{$categories->id}}" method="POST" enctype="multipart/form-data">
         @csrf
         <div class="container pt-5">
            <h1>{{ __('Edit category')}}</h1>
         <div class="mb-3">
             <label for="name" class="form-label">{{ __('Name')}}</label>
             <input type="text" class="form-control" id="name" name="name" value="{{ $categories->name }}">
           </div>


                 <button type="submit" class="btn btn-primary">Submit</button>
         </div>
    </form>
@endsection