<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Category;
use App\Models\Cart;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['categories']=Category::all();
        $data['products'] = Product::all();
        return view('products.index' ,$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories']=Category::all();
        return view('products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required',
            'category_id' => 'required',
        ]);

        $record = new Product;
        $record->name = $request->name;
        $record->price = $request->price;
        $record->category_id =$request->category_id;
        if($request->hasFile('image'))
    {
        $photo = $request->file('image');
        $path = 'uploads/products/'.time().'.'.$photo->extension();
        $photo->move(public_path('uploads/products/'), $path);
        $record->image_path= $path;
    }
        $record->save();

        Session::flash('success', 'Product has been created');
        return redirect()->route('products');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['categories']=Category::all();
        $data['products'] = Product::findOrFail($id);
        return view('products.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required',
            'category_id' => 'required',
        ]);

        $record = Product::findOrfail($id);
        $record->name=$request->name;
        $record->price=$request->price;
        $record->category_id=$request->category_id;
        if($request->hasFile('image'))
    {
        $photo = $request->file('image');
        $path = 'uploads/products/'.time().'.'.$photo->extension();
        $photo->move(public_path('uploads/products/'), $path);
        $record->image_path = $path;
    }
        $record->save();
        Session::flash('success', 'Product has been updated');
        return redirect()->route('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Product::findOrfail($id);
        $record->delete();
        return back();
    }

    public function search(Request $request)
    {
        $data = Product::
        where('name','like', '%'.$request->input('query').'%')
        ->get();
        return view('products.search',['products'=>$data]);
    }

    function addToCart(Request $request){

        if(Auth::check())
        {
            $cart= new Cart;
            $cart->user_id=Auth::user()->id;
            $cart->product_id=$request->product_id;
            $cart->save();

            return redirect('/index'); 
        }
        else
        {
            return redirect('/login');
        }
        
    }

    static function cartItem(){
        $userId = Auth::user()?->id;
        return Cart::where('user_id',$userId)->count();
    }

    function cartList()
    {
        //$userId = Auth::user()->id;
        $products = DB::table('carts')
        ->join('products','carts.product_id','=','products.id')
        ->where('user_id', Auth::user()->id)
        ->select('products.*','carts.id as cart_id')
        ->get();

        return view('products.cartlist',['products'=>$products]);
        
    } 

    function removeCart($id)
    {
       Cart::destroy($id);
       return redirect('products/cartlist');
    }

    function orderNow(){
        $total = $products = DB::table('carts')
        ->join('products','carts.product_id','=','products.id')
        ->where('user_id', Auth::user()->id)
        ->select('products.*','carts.id as cart_id')
        ->sum('products.price');

        return view('products.orders',['total'=>$total]);
        
    }
    function orderPlace(Request $request)
    {
        $allCart = Cart::where('user_id', Auth::user()->id)->get();
        foreach($allCart as $cart)
        {
            $record = new Order;
            $record->product_id=$cart['product_id'];
            $record->user_id=$cart['user_id'];
            $record->payment_method=$request->payment;
            $record->payment_status="pending";
            $record->address=$request->address;
            $record->save();
            //Cart::where('user_id', Auth::user()->id)->delete();
        }
        return redirect('/index');
    }

    public function order()
    {
        $data['orders']=Order::all();
        return view('products.allorders' ,$data);
    }
}
