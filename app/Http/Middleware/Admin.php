<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Closure;
use Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
    
    if(Auth::check())
    {
        if(Auth::user()->admin == '1'){

            return $next($request); 
        }

        else
        {
            return redirect('/home')->with('status','Access Denied! as you are not as admin');
        }
    }
    else
    {
        return redirect('/home')->with('Please Login First');
    }
}
}